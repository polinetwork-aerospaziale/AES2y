%% LAB08 - Ammortizzatore
close all

x_max = 0.3;
p0 = 3*1e+6;
v0 = 0.027;
rho = 780;
k_f = 1.6; %coeff perdite di carico in ciascun foro (valido anche per intaglio spina)
D_c =0.24; %diametro cilindro
D_s =0.1; %diametro spina
D_f =0.018; %diametro fori
n_f =4; %numero fori
A_max_s = 150*1e-06;

A_c = pi*(D_c^2)/4;
A_s = pi*(D_s^2)/4;


% Velocità in funzione della corsa
x = [0:0.001:0.3];
v = @(x) (-440*x.^2+70*x).*(x<0.08) + (-60*x.^2+10*x+2.368).*(x>=0.08);
figure
subplot(2,2,1)
plot(x,v(x))
grid on
xlabel('x')
ylabel('v')


A_f_tot = n_f*pi*(D_f^2)/4;
m1 = (A_max_s)/(0.07-0.05);
m2 = (-A_max_s)/(0.21-0.17);
A_leakage = @(x) A_f_tot.*(x<0.05) + (A_f_tot+m1*(x-0.05)).*(x>= 0.05 & x < 0.07) + (A_f_tot+A_max_s).*(x>= 0.07 & x < 0.17) + ((A_f_tot+A_max_s)+m2*(x-0.17)).*(x>= 0.17 & x < 0.21) + A_f_tot.*(x>= 0.21 & x <= 0.3);
subplot(2,2,2)
plot(x,A_leakage(x))
grid on
xlabel('x')
ylabel('A_{leakage}')

Q = @(x) (A_c-A_s)*v(x);
subplot(2,2,3)
plot(x,Q(x))
grid on
xlabel('x')
ylabel('Q')

v_f = @(x) Q(x)./A_leakage(x); %velocità nei fori
subplot(2,2,4)
plot(x,v_f(x))
grid on
xlabel('x')
ylabel('v_f')


%%
% Perdite di carico
delta_p = @(x) k_f*0.5*rho*(v_f(x)).^2;
figure
subplot(1,2,1)
plot(x,delta_p(x))
grid on
xlabel('x')
ylabel('\Delta p')


p_A = @(x) p0*(v0./(v0-A_c*x)).^1.4;
subplot(1,2,2)
plot(x,p_A(x))
grid on
xlabel('x')
ylabel('p_A')


%%
R_tot = @(x) delta_p(x)*(A_c-A_s) + p_A(x)*A_c;
figure
plot(x,R_tot(x),'LineWidth',2,'Color','b')
grid on
hold on
xlabel('x')
ylabel('R')
h = area(x,R_tot(x));
h.FaceColor = [0 0 1];
h.FaceAlpha = 0.25;
h.LineStyle = 'None';


R_visc = @(x) delta_p(x)*(A_c-A_s);
R_isoentr = @(x) p_A(x)*A_c;

plot(x,R_visc(x),'LineWidth',2)
plot(x,R_isoentr(x),'LineWidth',2)

plot( x, max(R_tot(x))*ones(length(x),1), '--k' )
legend('R_{tot}','Work','R_{visc}','R_{isoentr}')


work = simpcomp( min(x), max(x), length(x)-1, R_tot )
work_id = max(x)*max(R_tot(x));
etha = work/work_id











