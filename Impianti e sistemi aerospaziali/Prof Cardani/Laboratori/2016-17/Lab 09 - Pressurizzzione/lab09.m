close all

% LAB 09 - PRESSURIZZAZIONE

% Area valvola B

% dati
T_c=293.15; % temperatura
dz = 500;
z_e=[0:dz:12000]';
z_c_var = [0:dz/3:2000];
z_c=[z_c_var 2000*ones(1,length(z_e)-length(z_c_var))]';
p0 = 101357; n = 5.2561; a = 0.0065; T0 = 288.16;
Rs=287;


p4 = p0*1.2;
n_p = 75; %numero persone;
rho_c_0= p0/(Rs*T_c); %a quota zero ma in cabina
Q_pp = (0.25/60)/rho_c_0; %portata pro capite
Q = Q_pp*n_p;
p = @(z) p0*(1-a*z/T0).^n; 
p_c = p(z_c);
p_e = p(z_e);
delta_p_56_0 = p_c - p_e;

kB=6;

%% Dimensionamento a quota esterna zero

p5=p0;
p6=p0;

KA= (p4 - p5 )/Q^2; %coefficiente perdite di carivo valvola A

clear p5; clear p6

%% Quota variabile

rho_c= p_c./(Rs*T_c);

v56=sqrt((delta_p_56_0.*2)./(rho_c.*kB)); % velocità efflusso aria dalla cabina (in funzione della quota)

delta_p_45 = p4-p_c;
Q56 = sqrt(delta_p_45/KA); % portata in uscita in funzione della quota

A = Q56./v56;

A_B = max(A(2:end)); %escludo il primo valore=inf poichè temporaneamente errata; tra poco calcoliamo il valore vero


figure
subplot(1,2,1)
plot(z_e(2:end), v56(2:end))
xlabel('z_{ext}')
ylabel('v_{56} [m/s]')
grid on

subplot(1,2,2)
plot(z_e(2:end), Q56(2:end))
xlabel('z_{ext}')
ylabel('Q_{56} [kg/s]')
grid on


%% Correggiamo quanto scritto sinora, individuando cosa effettivamente accade a quota z_e = 0
% ed innfine possiamo plottare anxhe il profilo di pressione
delta_p_46_0 = p4 - p0;

Ktot = KA + kB*0.5*rho_c_0/(A_B^2);
Q46 = sqrt(delta_p_46_0/Ktot);

delta_p_45_0 = KA * Q46^2;

p_c_0 = p4 - delta_p_45_0;
p_c(1) = p_c_0; %corr.

z_c_0 = (-(p_c_0/p0)^(1/n) + 1)*T_c/a;
z_c(1) = z_c_0; %corr.


figure
plot(z_e, p(z_e))
hold on
grid on
plot(z_e, p_c)
xlabel('h')
ylabel('p')

legend('p_{e}', 'p_{c}')


% 
% delta_p_56_0 = p_c_0 - p0
% v56_0=sqrt((delta_p_56_0.*2)./(rho_c_0.*kB)); % velocità efflusso aria dalla cabina (in funzione della quota)
% v56(1) = v56_0; %corr.
% 
% Q56_0 = sqrt(delta_p_56_0/KA); % portata in uscita in funzione della quota
% Q56(1) = Q56_0; %corr.
% 
% A_0 = Q56_0/v56_0;
% A(1) = A_0; %corr.
% 
% figure
% subplot(1,2,1)
% plot(z_e, v56)
% xlabel('z_{ext}')
% ylabel('v_{56} [m/s]')
% grid on
% 
% subplot(1,2,2)
% plot(z_e, Q56)
% xlabel('z_{ext}')
% ylabel('Q_{56} [kg/s]')
% grid on



