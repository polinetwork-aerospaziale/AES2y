close all

%% EX 1
%% 1.1

s = tf('s')
G = 10*(1-s)/((1+s)*(1+0.1*s))

sys = ss(G);
figure
subplot(1,2,1)
bode(sys)
grid on
subplot(1,2,2)
nyquist(sys)
axis equal
grid on


%% 1.2
t = [0:.001:10];
u = sin(10*t);

[A,~,~,~] = ssdata(sys);
x0 = zeros(length(diag(A)),1);

y = lsim(sys, u, t, x0) %% Se ometto x0 lo assumo nullo



%% CON LA FUNZIONE DELLA STRADA
% omega_min = 10^-2;
% omega_max = 10^3;
% [num, den] = tfdata(G);
% plotbode(omega_min, omega_max, num{1}(2:end), den(1), 1)


%% 1.3
figure
omega = 10;
y = lsim(G,sin(omega*t),t)
grid on

G10 = freqresp(G,omega)
y_as = abs(G10)*sin(omega*t + angle(G10));
plot(t,y);
hold on
plot(t, y_as)
grid on

title('Verifica th r.i.f')