close all

A = [-4 -3 0 3; 0 -3 -1 1; 0 -2 -3 2; 0 -2 -1 0];
B = [0 0 0 1]';
c = [1 1 0 0];
D = 0;

sys = ss(A,B,c,D)

v = real(eig(A))

eigMaxRe = max(v);

x0 = [1 0 1 2]';

%% 1.1
if eigMaxRe < 0
   disp('Asymptotically Stable System') 
elseif eigMaxRe <= 0
   disp('Stable System')
else
   disp('Unstable System') 
end

%% 1.2
[y,t,x] = initial(sys, x0)
figure
subplot(1,2,1)
plot(t,y)
title('x0 = [1 0 1 2]'' Response')
grid on


%% OPPURE solo: initial(sys, x0)

%% 1.3
mu = dcgain(sys);
sprintf('DC Gain: %d', mu)

%% 1.4
[y,t,x] = step(sys);
subplot(1,2,2)
plot(t,y)
title('Step Response')
grid on


%% OPPURE solo: step(sys)




%% EX.2

r = 1; l = 1; c = 1;

%% 2.1

A = [0 0 1/c; 0 0 1/c; -1/l -1/l -r/l];
B = [0 0 1/l]';
C = [0 0 1];
D = 0;

sys = ss(A,B,C,D);
%% 2.2

K_r = ctrb(sys)
K_o = obsv(sys)

if det(K_r) == 0
    disp('Not Reachable')
else
    disp('Reachable')
end

if det(K_o) == 0
    disp('Not Observable')
else
    disp('Observable')
end

%% Per sistemi grandi conviene usare il rango per il pi? basso costo computazionale




%% 2.3
G = tf(sys)


% 2.4

T = [1 -1 0; 1 1 0; 0 0 1];
sys2 = ss2ss(sys, T);
[A2 B2 C2 D2] = ssdata(sys2);

K_r = ctrb(sys2)
K_o = obsv(sys2)

if det(K_r) == 0
    disp('Not Reachable')
else
    disp('Reachable')
end

if det(K_o) == 0
    disp('Not Observable')
else
    disp('Observable')
end

G2 = tf(sys2)




%% 3

% Let's define the laplace operator...
s = tf('s');
G1 = 3/(1+2*s);
G2 = 3*(1+s)/(1+2*s);
G3 = 3*(1-s)/(1+2*s);
G4 = 3/((1+10*s)*(1+s));
G5 = 3/(1+10*s);
G6 = 3/(1+10*s)^2;
G7 = 3*(1+30*s)/(1+10*s)^2;
G8 = 3*(1-30*s)/(1+10*s)^2;

figure
subplot(1,3,1)
hold on
step(G1)
step(G2)
step(G3)
grid on
legend('G_1', 'G_2', 'G_3')

subplot(1,3,2)
hold on
step(G4)
step(G5)
grid on
legend('G_4', 'G_5')


subplot(1,3,3)
hold on
step(G6)
step(G7)
step(G8)
grid on
legend('G_6', 'G_7', 'G_8')




%% EX.4

t_a = 10;
xi = 0.2

% t_a = 5 / (xi*puls)   -->
puls = 5 / (xi*t_a)

s = tf('s')
G = 3*(puls^2)/(s^2 + .4*puls*s + puls^2)

figure
step(G)
grid on

[y,t] = step(G)
overshootMax = max(y)

S = 100*exp(-xi*pi/sqrt(1-xi^2))