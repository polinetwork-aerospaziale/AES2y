clc
echo on
%
% Controllo del moto laterale in un Boeing 747
%
%

% Definizione del modello lineare per controllo laterale

A(1,:)=[-0.0558 -0.9968 0.0802 0.0415];
A(2,:)=[0.598 -0.115 -0.0318 0];
A(3,:)=[-3.05 0.388 -0.465 0];
A(4,:)=[0 0.0805 1 0];

b=[0.0729 -4.75 1.53 0; 0.0001 1.23 10.63 0]';

c=[0 1 0 0; 0 0 0 1];

b747=ss(A,b,c,0);
pause

%Autovalori del sistema

damp(b747)
pause
pzmap(b747)
pause

%Risposta all'impulso
impulse(b747,20)
pause

%Sottosistema
b747rr=b747(1,1);
pause

%Luogo delle radici inverso
rlocus(-b747rr),sgrid
pause

%Progetto del controllore
[k,p]=rlocfind(-b747rr);
pause

%Verifica in anello chiuso
clprop=feedback(b747,-k,1,1);
impulse(b747,clprop,20)
pause

%Progetto del circuito di washout
H=tf([1 0],[1 0.33]);
L=H*b747rr;
rlocus(-L),sgrid
pause

[k,p]=rlocfind(-L);
pause

%Verifica in anello chiuso
clwash=feedback(b747,-k*H ,1,1);
impulse(b747,clwash,20)