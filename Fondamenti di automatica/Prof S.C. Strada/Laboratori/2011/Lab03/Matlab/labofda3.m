clc
echo on

%%%
%%% ESERCIZIO 1
%%%

%Definizione dei parametri del sistema
M=1;
K=1;
D=0.4;
pause


%%%
%%% ESERCIZIO 2
%%%

%Definizione dei parametri del sistema
L=1;
R=2;
pause

%%%%
%%% ESERCIZIO 3
%%%

%Definizione della f.d.t. d'anello
L=tf(100,conv([1 2 1],[0.01 1]))
pause

%Calcolo del margine di fase e della pulsazione critica
margin(L)
pause

%Calcolo dello smorzamento csi a partire dal margine di fase fim

fim=5.8384;
csi=sin((fim/2)*(pi/180))
pause

%Approssimazione con un sistema del 2� ordine a poli complessi coniugati
mu=100/101;
wc=9.9253;
Fapp=tf(mu*wc^2,[1 2*csi*wc wc^2])
pause

%Confronto tra risposta sistema reale ed approssimato
F=feedback(L,1)
pause

step(F,Fapp)
