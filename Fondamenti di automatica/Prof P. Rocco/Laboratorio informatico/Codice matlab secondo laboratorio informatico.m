clc
clear all
close all

%% EX 1 - Controllo automatico dell'altitudine in un Boeing 747

%1.a) Definizione del modello lineare per controllo di altitudine

A = zeros(5,5);

A(1,:)=[-0.00643 0.0263 0 -32.2 0];
A(2,:)=[-0.0941 -0.624 820 0 0];
A(3,:)=[-0.000222 -0.00153 -0.668 0 0];
A(4,:)=[0 0 1 0 0];
A(5,:)=[0 -1 0 830 0];

b=[0 -32.7 -2.08 0 0]';

c=[0 0 0 0 1];

b747=tf(ss(A,b,c,0)) %funzione di trasferimento associata al ss model


%1.b) Poli del sistema. 

damp(b747) 


%1.c) Poli e zeri nel piano complesso
figure
pzmap(b747)


% Risposta allo scalino 
figure
step(b747)
title('Risposta allo scalino')

%per simulare la risposta a uno scalino di ampiezza e valore iniziale
%generico, occorre eseguire le seguenti istruzioni
% valore_iniziale = 0;
% ampiezza = 2;
% opt = stepDataOptions('InputOffset',valore_iniziale,'StepAmplitude',ampiezza);
% figure
% step(b747,opt);

% Risposta all'impulso
figure
impulse(b747)
title('Risposta impulso')

%Diagrammi di Bode 
figure
bode(b747)
grid on


%1.d) Chiusura di un anello di retroazione unitario
figure
margin(-b747);
grid on


%1.e) Progetto per tentativi del regolatore PD
% (valore di partenza: 1 tentativo di scelta dei parametri del regolatore)
KP=1e-5;
KD=10*KP;
figure
margin(series(b747,tf(-[KD KP],1)));
grid on

% Scelta finale dei parametri del regolatore
KP=5*1e-5;
KD=10*KP;
figure
margin(series(b747,tf(-[KD KP],1)));
grid on

%1.f) Verifica della risposta ottenuta 
% in anello chiuso ad un gradino di
% riferimento di altitudine

figure
step(feedback(series(b747,tf(-[KD KP],1)),1));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%5

%% EX.2 - Controllo del moto laterale in un Boeing 747
clc
clear all
close all

%2.1) Definizione del modello lineare per controllo laterale
A = zeros(4,4);

A(1,:)=[-0.0558 -0.9968 0.0802 0.0415];
A(2,:)=[0.598 -0.115 -0.0318 0];
A(3,:)=[-3.05 0.388 -0.465 0];
A(4,:)=[0 0.0805 1 0];

b=[0.0729 -4.75 1.53 0; 0.0001 1.23 10.63 0]';

c=[0 1 0 0; 0 0 0 1];

b747=ss(A,b,c,0)


%2.2) Poli del sistema

damp(b747)

%visualizzare singolarit� del sistema
figure
pzmap(b747)


%2.3) Risposta all'impulso relativa ai primi 20 s
figure
impulse(b747,20)

% Risposta all'impulso fino a transitorio esaurito
figure
impulse(b747)


%2.4) Selezione del sottosistema di interesse
b747rr=b747(1,1)


%2.5) 
%Verifichiamo che il guadagno del sistema sia negativo valutando la
%risposta a transitorio esaurito a uno scalino di ampiezza unitaria
figure
step(b747rr) %la risposta a transitorio esaurito � circa -153 -> guadagno negativo
% alternativamente usare: 
dcgain(b747rr)

% Luogo delle radici inverso
figure
rlocus(-b747rr),sgrid


%2.6) Progetto del controllore
[k,p]=rlocfind(-b747rr)


%2.7) Verifica in anello chiuso
clprop=feedback(b747,-k,1,1);
figure
impulse(b747,clprop,20)


%2.8) Progetto del circuito di washout
H=tf([1 0],[1 0.33]);
L=H*b747rr;
figure
rlocus(-L),sgrid


[k,p]=rlocfind(-L)


%2.9) Verifica in anello chiuso
clwash=feedback(b747,-k*H ,1,1);
figure
impulse(b747,clwash,20)