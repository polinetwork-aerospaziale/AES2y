clc


%%%
%%% ESERCIZIO 1
%%%

%Definizione del sistema dinamico
A=[-4,-3,0,3;0,-3,-1,1;0,-2,-3,2;0,-2,-1,0];
B=[0;0;0;1];
C=[1,1,0,0];
s1=ss(A,B,C,0)

		
%Verifica della stabilit�
eig(A)
eig(s1)

%Il sistema � asintoticamente stabile


%Calcolo del moto libero
x0=[1;0;1;2];
figure
initial(s1,x0) %risposta dell'uscita

%risposta degli stati
[y,t,x] = initial(s1,x0);
figure; grid on; hold on;
plot(t,x(:,1),'b',t,x(:,2),'r',t,x(:,3),'g',t,x(:,4),'k');
legend('x1','x2','x3','x4');



%Calcolo del guadagno statico
dcgain(s1)


-C*inv(A)*B


%Calcolo della risposta allo scalino
figure
step(s1)



%%%
%%% ESERCIZIO 2
%%%


%Definizione del sistema dinamico
A=[0,0,1;0,0,1;-1,-1,-1];
B=[0;0;1];
C=[0,0,1];
s2=ss(A,B,C,0)


%Matrice di raggiungibilit�
Kr=ctrb(s2);
det(Kr)
%Il sistema non � completamente raggiungibile

%autovalori del sistema
%eig(s2)



%Matrice di osservabilit�
Ko=obsv(s2);
det(Ko)
%Il sistema non � completamente osservabile


%Funzione di trasferimento
tf(s2)

%La f.d.t. � quella di un sistema del secondo ordine (a parte l'errore numerico)


%Prendiamo come nuove variabili somma e differenza delle tensioni sui condensatori e la corrente sull'induttore

T=[1,-1,0;1,1,0;0,0,1];
s2new=ss2ss(s2,T);
[A2,B2,C2,D2]=ssdata(s2new)

%riusciamo a isolare la variabile di stato che non influenza il legame
%ingresso uscita, ovvero la G(s), rappresentazione esterna del sistema.
%La prima variabile di stato non contribuisce al legame ingresso-uscita



tf(s2new)
%La funzione di trasferimento � la stessa


%%%%
%%% ESERCIZIO 3
%%%

G1=tf(3,[2 1])
G2=tf(3*[1 1],[2 1])
G3=tf(3*[-1 1],[2 1])

figure
step(G1,G2,G3)
legend()

%------

G4=tf(3,[10 11 1])
G5=tf(3,[10 1])

figure
step(G4,G5)
legend()

%------

G6=tf(3,[100 20 1])
G7=tf(3*[30 1],[100 20 1])
G8=tf(3*[-30 1],[100 20 1])

figure
step(G6,G7,G8)
legend()



%%%
%%% ESERCIZIO 4
%%%

%Definizione del sistema dinamico
 G=tf(10*[-1 1],conv([1 1],[0.1 1])) %conv(u,v) esegue la moltiplicazione tra i coefficienti dei polinomi contenuti in u e v

 
%Tracciamento dei diagrammi di Bode
figure 
bode(G)


%Risposta alla sinusoide tramite comando lsim
t=0:0.01:10;
y=lsim(G,sin(10*t),t);
figure
plot(t,y)


%Calcolo della risposta in frequenza per w=10 
G10=freqresp(G,10)
modulo = abs(G10)
fase = angle(G10)


%Calcolo risposta asintotica e confronto
yas=abs(G10)*sin(10*t+angle(G10));
figure;
plot(t,y,t,yas);
%le risposte coincidono a transitorio esaurito

%Tracciamento del diagramma polare
figure
nyquist(G)


