% potenze in regime sinusoidale
clear all; close all; clc

%% dati

f = 50;     % Hz
w = 2*pi*f; % rad/s
t = (0:.0002:.05)';

Vo1   = 5;    % ampiezza della tensione di fase (dominio del tempo)
phi1 = 0;     % fase della sinusoide della tensione (dominio del tempo)
V1   = Vo1/sqrt(2)*exp(1i*phi1); %fasore

Vo2   = Vo1;
phi2 = phi1-2/3*pi;
V2   = Vo2/sqrt(2)*exp(1i*phi2);

Vo3   = Vo1;
phi3 = phi2-2/3*pi;
V3   = Vo3/sqrt(2)*exp(1i*phi3);

Z    = 3+4j;    % impedenza in ohm; carico equilibrato

modZ = abs(Z);  % modulo dell'impedenza
phiZ = angle(Z);% fase dell'impedenza

I1   = V1 / Z;   % ampiezza della sinusoide della corrente (nel tempo)
I2   = V2 / Z;
I3   = V3 / Z;

Io1  = abs(I1)*sqrt(2);
Io2  = abs(I2)*sqrt(2);
Io3  = abs(I3)*sqrt(2);

%% torniamo nel dominio del tempo

vt1 = Vo1 * cos(w*t + phi1);  % tensione nel dominio del tempo (V)
vt2 = Vo2 * cos(w*t + phi2);  % tensione nel dominio del tempo (V)
vt3 = Vo3 * cos(w*t + phi3);  % tensione nel dominio del tempo (V)

it1 = Io1 * cos(w*t + phi1-phiZ);  % corrente nel dominio del tempo (A)
it2 = Io2 * cos(w*t + phi2-phiZ);  % corrente nel dominio del tempo (A)
it3 = Io3 * cos(w*t + phi3-phiZ);  % corrente nel dominio del tempo (A)

figure(1)
set(gcf,'position',[200 200 960 480])
hold on
h1=plot(t,[vt1 vt2 vt3],'Linewidth',3);
h1=plot(t,[it1 it2 it3],'--','Linewidth',2);
legend('v_1(t)','v_2(t)','v_3(t)','i_1(t)','i_2(t)','i_3(t)',4)
plot(t,0*t,'k'), grid
ampMax = max([Vo1 Io1 Vo1*Io1]);
axis([t(1) t(end) -ampMax ampMax])
xlabel('t (s)')
title(['Tensione e corrente istantanee [Z = (' num2str(Z) ') \Omega]'])


%% calcoliamo la potenza istantanea 

pt1 = vt1 .* it1;              %  potenza nel dominio del tempo (W)
pt2 = vt2 .* it2;              %  potenza nel dominio del tempo (W)
pt3 = vt3 .* it3;              %  potenza nel dominio del tempo (W)

Ptt = pt1 + pt2 + pt3;  % espediente per avere un vettore uguale a Pm

figure(2)
set(gcf,'position',[200 200 960 480])
hold on
h1=plot(t,[pt1 pt2 pt3 Ptt],'Linewidth',3);
set(h1(4),'color','r','LineStyle','--')
legend(h1,'p_1(t)','p_2(t)','p_3(t)','P_{tot}(t)',4)
plot(t,0*t,'k'), grid
ampMax = max([Vo1 Io1 Vo1*Io1]);
axis([t(1) t(end) -ampMax ampMax])
xlabel('t (s)')
title(['Potenza istantanea p(t) e totale P_{tot} in funzione di p_1(t), p_2(t) e p_3(t) [Z = (' num2str(Z) ') \Omega]'])

