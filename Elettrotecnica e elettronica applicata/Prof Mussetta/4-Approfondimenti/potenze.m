% potenze in regime sinusoidale
clear all; close all; clc

%% dati

f = 50;     % Hz     (frequenza)
w = 2*pi*f; % rad/s  (pulsazione omega)
t = (0:.0002:.05)';% (tempo)

Vo   = 5;    % ampiezza della sinusoide della tensione (dominio del tempo)
phiv = 0; % fase della sinusoide della tensione (dominio del tempo)
R    = 5;    % Ohm
L    = 0.02; % H
C    = 0; % F 

%% entriamo nel dominio dei fasori 

XL   = w*L;
if C>0
    XC   = -1/(w*C);
else
    XC = 0;
end
Z    = R + 1j*(XL+XC);    % impedenza in ohm (ipotesi: 3 bipoli in serie)

modZ = abs(Z);  % modulo dell'impedenza
phiZ = angle(Z);% fase dell'impedenza

Veff = Vo / sqrt(2);     % valore efficace della V (modulo del fasore)
Ieff = Veff / modZ;      % valore efficace della I (modulo del fasore)
phii = phiv - phiZ;      % fase della sinusoide della corrente (nel tempo)

%% torniamo nel dominio del tempo

Io   = Ieff * sqrt(2);   % ampiezza della sinusoide della corrente (nel tempo)

vt = Vo * cos(w.*t + phiv);  % tensione nel dominio del tempo (V)
it = Io * cos(w.*t + phii);  % corrente nel dominio del tempo (A)

%% calcoliamo la potenza istantanea 

pt = vt .* it;              %  potenza nel dominio del tempo (W)

modA  = Veff*Ieff;   % potenza APPARENTE (VA)
modAt = modA + t*0;  % espediente per avere un vettore uguale a modA

Pm    = modA*cos(phiZ);  % potenza media (W) = POTENZA ATTIVA
Pmt   = Pm + t*0; % espediente per avere un vettore di elementi uguali a Pm

Q     = modA * sin(phiZ);       % potenza REATTIVA
Qt    = Q + t*0;  % espediente per avere un vettore di elementi uguali a Q

cosphi = Pm/modA;


figure(1)
set(gcf,'position',[200 200 960 480])
area(t,pt,'facecolor',[1 .5 .5])
hold on
h1=plot(t,[vt it pt Pmt modAt],'Linewidth',3);
set(h1(4),'color',[0 0.5 0],'LineWidth',2,'LineStyle','-.')
set(h1(5),'color','k','LineWidth',3,'LineStyle',':')
legend(h1,'v(t)','i(t)','p(t)','P_m','|A|',4)
plot(t,0*t,'k'), grid
ampMax = max([Vo Io Vo*Io]);
axis([t(1) t(end) -ampMax ampMax])
xlabel('t (s)')
title(['Potenza istantanea p(t) e media P_m in funzione di v(t) e i(t) [Z = (' num2str(Z) ') \Omega] '])


compP = Pm * (1+cos(2*w*t + 2*phii));
compQ = Q  * (-sin(2*w*t + 2*phii));
ptemp = compP + compQ;                 % potenza ISTANTANEA

figure(2)
set(gcf,'position',[200 200 960 480])
area(t,ptemp,'facecolor',[1 .5 .5])
hold on
h1=plot(t,[compP compQ ptemp Pmt Qt modAt ],'Linewidth',3);
set(h1(4),'color',[0 0.5 0],'LineWidth',2,'LineStyle','-.')
set(h1(5),'color','r','LineWidth',2,'LineStyle','--')
set(h1(6),'color','k','LineWidth',3,'LineStyle',':')
legend(h1,'V_{eff}I_{eff}cos\phi\cdot(1+cos(2\omegat+2\phi_i))','-V_{eff}I_{eff}sin\phi\cdotsin(2\omegat+2\phi_i)','p(t)','P = P_m','Q','|A|',4)
plot(t,0*t,'k'), grid
axis([t(1) t(end) -ampMax ampMax])
xlabel('t (s)')
title(['Potenza istantanea p(t) e media P_m in funzione di P e Q [Z = (' num2str(Z) ') \Omega;  cos(\phi) = ' num2str(cosphi) ']'])