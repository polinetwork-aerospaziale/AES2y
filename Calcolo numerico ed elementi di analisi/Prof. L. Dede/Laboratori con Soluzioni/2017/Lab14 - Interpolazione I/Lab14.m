
%% EX. 1
close all


fig1 = figure(1);
fig2 = figure(2);

f = @(x) x.*sin(x);
a = -2; b = 6;
x_axis = [a:0.001:b];
set(0,'CurrentFigure',fig1)
plot(x_axis,f(x_axis));
hold on




e_n_vect = [];
for n = 2:2:6

    x = linspace(a,b,n+1);
    y = f(x);

    p = polyfit(x,y,n);
    p_y = polyval(p,x_axis);

    set(0,'CurrentFigure',fig1)
    plot(x_axis, p_y)
    hold on
    grid on
    
    
    set(0,'CurrentFigure',fig2)
    E_n = @(x) abs(f(x_axis) - p_y);
    plot(x_axis,E_n(x));
    hold on
    grid on
    
    e_n = max(abs( f(x_axis) - p_y));
    e_n_vect = [e_n_vect e_n];
end
    
set(0,'defaultTextInterpreter','latex')

set(0,'CurrentFigure',fig1)
legend('f(x)','\Pi_{2}','\Pi_{4}','\Pi_{6}');

set(0,'CurrentFigure',fig2)
legend('E_{2}(x)','E_{4}(x)','E_{6}(x)');



%% EX.2.1,2.2
close all

fig1 = figure(1);
fig2 = figure(2);

n_vect = [2 4 8 10];

f = @(x) sin(1./(1+x.^2));
a = -2*pi; b = 2*pi;
x_axis = [a:0.001:b];

set(0,'CurrentFigure',fig1)
plot(x_axis,f(x_axis));
hold on

e_n_vect = [];
for k = 1:length(n_vect)
    n = n_vect(k);
    
    x = linspace(a,b,n+1);
    y = f(x);
    
    %
    
    p = polyfit(x,y,n);
    p_y = polyval(p,x_axis);
    
    %
    
    set(0,'CurrentFigure',fig1)
    plot(x_axis, p_y)
    hold on
    grid on
    
    set(0,'CurrentFigure',fig2)
    E_n = @(x) abs(f(x_axis) - p_y);
    plot(x_axis,E_n(x));
    hold on
    grid on
    
    e_n = max(abs( f(x_axis) - p_y));
    e_n_vect = [e_n_vect e_n];
end

set(0,'CurrentFigure',fig1)
legend('f(x)','\Pi_{2}','\Pi_{4}','\Pi_{8}','\Pi_{10}');

set(0,'CurrentFigure',fig2)
legend('E_{2}(x)','E_{4}(x)','E_{8}(x)','E_{10}(x)');

%% EX. 2.3 (NODI CGS)

close all

fig1 = figure(1);
fig2 = figure(2);

n_vect = [2 4 8 10];

f = @(x) sin(1./(1+x.^2));
a = -2*pi; b = 2*pi;
x_axis = [a:0.001:b];

set(0,'CurrentFigure',fig1)
plot(x_axis,f(x_axis));
hold on

e_n_vect = [];
for k = 1:length(n_vect)
    n = n_vect(k);
    
    x_CGS = [];
    for i = 0:n
        x_CGS_i = (a+b)/2 + ((b-a)/2)*cos(i*pi/n);
        x_CGS = [x_CGS x_CGS_i];
    end
    y_CGS = f(x_CGS);
    
    %
   
    p = polyfit(x_CGS,y_CGS,n);
    p_y = polyval(p,x_axis);
    
    %
    
    set(0,'CurrentFigure',fig1)
    plot(x_axis, p_y)
    hold on
    grid on
    
    set(0,'CurrentFigure',fig2)
    E_n = @(x) abs(f(x_axis) - p_y);
    plot(x_axis,E_n(x));
    hold on
    grid on
  

    e_n = max(abs( f(x_axis) - p_y));
    e_n_vect = [e_n_vect e_n];
end

set(0,'CurrentFigure',fig1)
legend('f(x)','\Pi_{2}','\Pi_{4}','\Pi_{8}','\Pi_{10}');

set(0,'CurrentFigure',fig2)
legend('E_{2}(x)','E_{4}(x)','E_{8}(x)','E_{10}(x)');
