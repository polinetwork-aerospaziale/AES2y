close all


detVect = [];
nVect = [1:15]

for n = 1:length(nVect)
    
    H = zeros(n);

    for i=1:n
        for j=1:n

            H(i,j) = 1/(i+j-1);

        end

    end

    H

    H - hilb(n)

    detVect = [detVect, det(H)]

end


semilogy(nVect, detVect);
grid on


polinomial = polyfit(nVect, detVect,5)

