%labo 14 ese 1
clear all
close all
clc

%domanda 1
A=randint(100,100);
s=sum(A);

for i=1:size(A,1)
    A(i,:)=A(i,:)./s;
end

%domanda 2
B=[0     0     0     1     0
     1     0     0     0     0
     0     1     0     0     0
     0     1     0     0     1
     1     1     1     1     0];
 
 s2=sum(B);
 
 for j=1:size(B,1)
     B(j,:)=B(j,:)./s2;
 end
 
 %domanda 3
 [lambdaA,x1 ,iter1]=eigpower(A);
 [lambdaB,x2,iter2]=eigpower(B);

 %domanda 4
 [lambdaA2,x3,iter3]=eigpower(A)
 [lambdaB2,x4,iter4]=eigpower(B)