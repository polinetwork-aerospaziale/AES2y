%labo8ese1
clear all
close all
clc

%faccio la matrice
k=100;
l=20;
N=20;
n=N-1;
%assemblo
diagonaleesterna=ones(n-1,1);
diagonaleprinc=-2*ones(n,1);
A=diag(diagonaleprinc,0)+diag(diagonaleesterna,1)+diag(diagonaleesterna,-1);
A=k*A;
%vettore termine noto
tnoto=zeros(n,1);
tnoto(end)=tnoto(end)-k*l;
%algoritmo prima volta
[L,U,X]=thomas(A,tnoto)
figure(1)
plot(X)

%algoritmo seconda volta
tnoto2=16*rand(n,1);
tnoto2(end)=tnoto2(end)-k*l;
[L1,U1,Y1]=thomas(A,tnoto2)
figure(2)
plot(Y1)