%Labo7

clear all
close all
clc


A=[50 1 3;1 6 0;3 0 1];
B=[50 1 10;3 20 1;10 4 70];
C=[7 8 9;5 4 3 ;1 2 6];
%controllo a se � simmetrica 
if (A==A')
    disp('A=simmetrica');
    na=size(A,1);
    for i=1:na
      if(det(A(1:i,1:i))>0)
          if(i==na)
              disp('A=definita positiva')
          end
      end
    end
end
%Controllo la dominanza diagonale di B
db=diag(B);
nb=size(B,1);
for j=1:nb
    r(j)=abs(db(j))-sum(abs(B([1:j-1,j+1:nb], j)));
end
if(r>0)
    disp('B=dominanza diagonale')
end

%verifico i determinanti dei minori di C
i=1;
dc=1;
nc=size(C,1);
while ((i<nc)&& (dc~=0))
    dc=det(C(1:i,1:i));
    i=i+1;
end
if (dc~=0)
    disp('C=possono applicare lU')
end

%applico la decomposizione

[La,Ua]=lugas(A);
[Lb,Ub]=lugas(B);
[Lc,Uc]=lugas(C);

%sostituizioni in avanti e indietro
sol=ones(3,1);
b=A*sol;
ya=forward(La,b);
xa=backward(Ua,ya);

errorerelativo=norm(sol-xa)/norm(xa);
residuo=norm(b-A*xa)/norm(b);
