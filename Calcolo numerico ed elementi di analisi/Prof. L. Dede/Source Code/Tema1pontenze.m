function [l,x,it]=Tema1pontenze(A,x0,toll,nmax)

if nargin ==2
    toll=1e-5;
    nmax=1000;
end

y=x0/norm(x0);
l=y'*A*y;
err=toll*abs(l)+1;
it=0;

while(err>toll*abs(l))&&(it<nmax)
    x=A*y;
    y=x/norm(x);
   
    err=abs(y'*A*y-l);
    l=y'*A*y;
    it=it+1;
end

if it>=nmax
    disp('il metodo non converge')
end


