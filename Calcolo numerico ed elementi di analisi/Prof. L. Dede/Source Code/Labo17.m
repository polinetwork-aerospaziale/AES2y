%%labo17
clear all
close all
clc
%domanda 1
l=-42;
y0=2;
t0=0;
tmax=1;

xval=[0:0.001:tmax];
f1=@(x)y0*exp(l*x);
y=f1(xval);

figure(1);
plot(xval,y)

%domanda 2
f=@(t,y)l*y;
h1=0.05;
[t1,u1]=euleroavanti(f,tmax,y0,h1);
[t2,u2,it2]=euleroindietro(f,tmax,y0,h1);

figure(2)
plot(t1,u1,'g')
hold on
plot(xval,y)
figure(3)
plot(t2,u2)
hold on
plot(xval,y,'r')

%domanda 3
h2=1.2/42;

[t3,u3]=euleroavanti(f,tmax,y0,h2);
[t4,u4,it4]=euleroindietro(f,tmax,y0,h2);

figure(4)
plot(t3,u3,'g')
hold on
plot(xval,y,'r')

figure(5)
hold on
plot(xval,y,'r')
plot(t4,u4,'b')

%domanda 4
h3=0.7/42;
[t5,u5]=euleroavanti(f,tmax,y0,h3);
[t6,u6,it6]=euleroindietro(f,tmax,y0,h3);

figure(6)
plot(t5,u5,'g')
hold on
plot(xval,y,'r')

figure(7)
hold on
plot(xval,y,'r')
plot(t6,u6,'b')

%%ESERCIZIO 2
close all
%domanda 2
h6=0.02;
[t,u,it]=Crank_Nicolson2(f,tmax,y0,h6);
figure(1)
plot(xval,y)
hold on
plot(t,u,'^r')
%domanda 3

figure(2)
plot(it,'*')
%domanda 4
N=5;
d=2.^(1:N);
passi=(0.04)./d;


for it=1:N
      [EI_t_h,EI_u_h]=euleroindietro(f,tmax,y0,passi(it));
      [CN_t_h,CN_u_h]=Crank_Nicolson2(f,tmax,y0,passi(it));
      yh=f1(0:passi(it):1);
      CN(it)=max( abs (yh-CN_u_h) );
      EI(it)=max( abs (yh-EI_u_h) );
end

figure(10)
loglog(passi,CN,'*');
hold on
loglog(passi,passi,'g');
loglog(passi,passi.^2,'r');
loglog(passi,EI,'y');
legend('crank nick','ordine 1','ordine 2','euleroindietro')
grid on
