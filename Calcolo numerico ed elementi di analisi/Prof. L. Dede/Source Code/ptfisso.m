function [x,it]=ptfisso(x0,nmax,toll,phi)
x=x0;
err=toll+1;
k=0;
while(err>toll && k<nmax)
    new=phi(x);
    err=abs(new-x);
    x=new;
    k=k+1;
end

it=k;