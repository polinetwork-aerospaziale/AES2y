function I = gausscomp(a,b,N,fun)

h=abs(a-b)./N;
x=[a:h:b];
xm=[a+h/2:h:b];%punti medi

x1=xm-(h/(2*3.^0.5));%parte 1 dei nodi
x2=xm+(h/(2*3.^0.5));%parte 2 dei nodi

y1=fun(x1);%valuto le aree
y2=fun(x2);

I=(h/2).*sum(y1);
I=I+(h/2).*sum(y2);%integrale