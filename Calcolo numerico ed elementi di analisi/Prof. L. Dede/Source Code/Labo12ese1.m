%Labo 12 ese 1
clear all
close all
clc
%domanda 1
a=-2;
b=+6;
f=@(x)x.*(sin(x));
x=linspace(a,b,1000);
y=f(x);
figure(1)
plot(x,y)
grid on

%domanda 2
%polinomio di lagrange

PPdis=[];
errdis=[];
errmax=[];

for n=2:2:6
    h=(b-a)/n;
    xnodo=(a:h:b);
    fnodo=f(xnodo);
    
    P=polyfit(xnodo,fnodo,n);
    Pdis=polyval(P,x);
    
    PPdis=[PPdis;Pdis];
    errdis=[errdis;abs(Pdis-y)];
    errmax=[errmax;max(abs(Pdis-y))];
end

figure(2)
plot(x,y,x,PPdis)
legend('y','l2','l4','l6')
grid on

%domanda 3
figure(3)
plot(x,errdis)
grid on

errmax