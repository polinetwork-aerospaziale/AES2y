function [t_h,u_h]=euleroavanti(f,t_max,y_0,h)

t0=0;
t_h=[t0:h:t_max];

N_istanti_temp=length(t_h);
u_h=zeros(1,N_istanti_temp);

% ciclo iterativo che calcola u (n+1)=u n+h*f n
u_h(1)=y_0;

for i=2:N_istanti_temp
    
    u_old=u_h(i-1);
    u_h(i)=u_old+h*f(t_h(i-1),u_old);
    
end