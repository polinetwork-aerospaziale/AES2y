function [l,it]=qr1(A,toll,nmax)
err=max(max(tril(A,-1)));
it=0;

while(err>toll && it<nmax)
    [Q,R]=qr(A,0);
    A=R*Q;
    err=max(max(abs(tril(A,-1))));
    it=it+1;
end

l=diag(A);