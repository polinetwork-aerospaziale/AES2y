function[xnew,k]=jacobi(A,b,x0,toll)
%imposto l'ingresso
nmax=1000;
if nargin==3;
    toll=1e-6;
end
%inizio le quantita
n=length(b);
xnew=zeros(n,1);
k=0;%numero di iterazioni

%controlli
if ((size(A,1)~=n)||(size(A,2)~=n)||(length(x0)~=n))
  error('dimensioni errate')
end
if (prod(diag(A))==0)
  error('elementi nulli')
end
%algoritmo vero e proprio
Din=diag(1./diag(A));
xv=x0;
r0=b-A*x0;
r=r0;
err=norm(r)/norm(r0);

while err>toll && k<nmax
    k=k+1;
    z=Din*r;
    xnew=xv+z;
    r=b-A*xnew;
    err=norm(r)/norm(r0);
    xv=xnew;
end
