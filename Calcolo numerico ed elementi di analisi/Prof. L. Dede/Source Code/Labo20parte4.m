%% Labo 20 parte 3
clear all
close all
clc

%% domanda 1

Q=0;
aa=0;
bb=1;

forzante=@(x,t)(-sin(t)+0.25*cos(t))*sin(x'/2);
uesatta=@(x,t)sin(x./2)*cos(t);

us=@(t)uesatta(0,t);
ud=@(t)uesatta(1,t);
u0=@(x)uesatta(x,0);

%% Eulero implicito
teta=1;
n=100;
h=(bb-aa)/n+1;
T=1;
deltat=0.1;

[U,griglia_x,passi_t]=diff_fin(Q,forzante,aa,bb,us,ud,u0,T,n,deltat,teta,1);


%% punto 2 uso Eulero Esplicito

teta2=0;
L=1;
n=100;
h=L/(n+1);
T=1;
deltat=0.1;

%% punto 3



theta=1;
if theta>=0.5 
    % caso Eulero Implicito e Crank Nicolson
    %NB: il metodo e' stabile e mi permette di spingere di piu' sulla discretizzazione
      n=10;
      h=L/(n+1);
      deltat0=0.2;
      T=1;
else
    %caso Eulero Esplicito
    %NB: non posso infittire troppo in x, altrimenti devo usare dei passi dt proibitivi
      n=10;
      h=L/(n+1);
      deltat0=0.002;
      T=1;
end


%verifica della condizione di stabilita' per EA

%u_esatta=@(x,t) (1+x-x.^4)'*t^4;

for i=1:6
      i
      deltat(i)=deltat0/2^i;
      [U,griglia_x,passi_t] = diff_fin(Q,forzante,L,us,ud,u0,T,n,deltat(i),theta,0);
      u=U(:,end);
      u_ex=u_esatta(griglia_x,passi_t(end));
      errore(i)=max(abs(u_ex-u));
end

figure
loglog(deltat,errore,'-ok','LineWidth',2);
hold on
plot(deltat,1/5000*deltat,'--','LineWidth',2)
plot(deltat,1/500*deltat.^2,'--r','LineWidth',2)

xlabel('\Deltat')
ylabel('errore')
title('convergenza dell''errore di approssimazione','fontsize',16)
legend('errore','\Deltat','\Deltat^2',2)



if (salva==1) 
    switch theta
        case 1
            print('errore_temporale_EI.eps','-depsc2')
        case 0
            print('errore_temporale_EA.eps','-depsc2')
        case 0.5
            print('errore_temporale_CN.eps','-depsc2')
        otherwise
    end
end
