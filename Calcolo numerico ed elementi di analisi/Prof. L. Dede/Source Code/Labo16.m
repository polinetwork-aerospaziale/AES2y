%%labo 16
clear all
close all
clc

%esercizio 1
y0=0.1;
l=2.4;
t_max=3;

t_plot=[0:0.1:t_max];

y=@(x)y0*exp(l*x);
f=@(t,y)l*y;

h1=0.05;
h2=0.01;
%domanda 1
[t1,u1]=euleroavanti(f,t_max,y0,h1);
[t2,u2]=euleroavanti(f,t_max,y0,h2);

%domanda 2
figure(1)
plot(t_plot,y(t_plot),'Linewidth',2);
hold on
grid on
plot(t1,u1,'og','MarkerSize',5);
plot(t2,u2,'^r','MarkerSize',5);
legend('analitica','passo grosso','passo fine')

%domanda 3
h1 = 0.05;
[th3,uh3,iter3] = euleroindietro(f,t_max,y0,h1);
h2 = 0.01;
[th4,uh4,iter4] = euleroindietro(f,t_max,y0,h2);

%domanda 4
figure(2)
plot(t_plot,y(t_plot),'Linewidth',5);
hold on
grid on
plot(th3,uh3,'og','MarkerSize',5);
plot(th4,uh4,'^r','MarkerSize',5);
legend('analitica','passo grosso','passo fine')

%domanda 5
figure(3);
plot(th3,iter3,'o:','MarkerSize',4)

%domanda 6
c=5;
s=2.^(1:c);
passi=0.08./s;


for j=1:c
    [EIT,EIU,EITER] = euleroindietro(f,t_max,y0,passi(j));
    [EAT,EAU]=euleroavanti(f,t_max,y0,passi(j));
    y_h=y( 0 : passi(j) : t_max );
    EEA(j)=max(abs(y_h-EAU));
    EEI(j)=max(abs(y_h-EIU));

end

figure(4)
loglog(passi,EEA,passi,EEI)
legend('EA','EI')
