%labo 10
clear all
close all
clc
%ese1

%domanda 1
n=50;
n2=49;
n3=48;
n4=47;
n5=46;

d1=diag(4*ones(n,1));
d2=diag(-1*ones(n2,1),+1);
d3=diag(-1*ones(n2,1),-1);
d4=diag(-1*ones(n3,1),+2);
d5=diag(-1*ones(n3,1),-2);
A=d1+d2+d3+d4+d5;

b=0.2*ones(n,1);

toll=1e-5;
x0=ones(n,1);
nmax=10000;

%domanda 2
if A==A'
    disp('E'' SDP')
else
    disp('Non � SDP')
end
e=eig(A);
cond=max(e)/min(e);

%domanda 3 richardson
%domanda 4
P=eye(n);
a1=0.2;
[x,it]=richy(A,b,P,x0,toll,nmax,a1);

a2=0.33;
[x2,it2]=richy(A,b,P,x0,toll,nmax,a2);

a3=2/max(e)+min(e);
[x3,it3]=richy(A,b,P,x0,toll,nmax,a3);

[x4,it4]=richy(A,b,P,x0,toll,nmax);

Balfa=eye(n)-a1*A;
max(abs(eig(Balfa)))

Balfa=eye(n)-a2*A;
max(abs(eig(Balfa)))

%domanda5
P5=tril(A);
a5=1;
Ba5=eye(n)-A*inv(P)*a5;
Rs=max(abs(eig(Ba5)));

[x5,it5]=richy(A,b,P5,x0,toll,nmax);

