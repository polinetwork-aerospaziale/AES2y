function[th,uh]=Tema2Ei(f,T,y0,h)
t0=0;
th=t0:h:T;

N=length(th);
uh=zeros(1,N);
uh(1)=y0;

%parametri di iterazione punto fisso
nmax=100;
toll=1e-5;
iter_pf=zeros(1,N);

for it=2:N
    u_old=uh(it-1);
    t_pf=th(it);
    phi=@(u)u_old+h*f(t_pf,u);
    [upf, itpf]=puntofissoprof(u_old, phi, nmax, toll);
    uh(it)=upf(end);
    iter_pf(it)=itpf;
end