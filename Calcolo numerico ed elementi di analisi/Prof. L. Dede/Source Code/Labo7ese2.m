%labo7ese2

clear all
close all
clc

A=[50 3 1;1 6 0;3 0 1];
n=size(A,1);

[L,U]=lugas(A);

I=eye(n);
Ainv=[];

for i=1:n
    yn=forward(L,I(:,i));
    xn=backward(U,yn);
    Ainv=[Ainv,xn];
end