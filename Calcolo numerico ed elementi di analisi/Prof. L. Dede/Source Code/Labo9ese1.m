%labo9ese1
clear all
close all
clc

%ese
A=hilb(1000);
B=rand(1000);

n=size(A,1);

x1=ones(n,1);
y1=ones(n,1);

b=A*x1;
c=B*y1;

x=A\b;
y=B\c;


E1=norm(x-x1)/norm(x1)
E2=norm(y-y1)/norm(y1)