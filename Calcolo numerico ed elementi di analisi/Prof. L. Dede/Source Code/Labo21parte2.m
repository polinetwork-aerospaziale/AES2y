%% Labo 21 parte 2

clear all
close all
clc

%% Domanda 1 con newman del secondo ordine

uesatta=@(x)sin(x./2).*cos(x);
duesatta=@(x)0.5.*cos(x./2)*cos(x)-sin(x./2).*sin(x);

q=3;
a=-4;
b=+4;

forzante=@(x)(5/4+q).*sin(x./2).*cos(x)+cos(x./2).*sin(x);
us=uesatta(a);
l=duesatta(b);

H=[0.5 0.25 0.125 0.0625 0.03125];
err=[];

for h=H
    n=(8/h)-1;
    xn=-4:h:4;
    
    du=-ones(n-1,1)/h^2;
    dl=du;
    d0=(2+q*h^2)*ones(n,1)/h^2;
    
    b=forzante(xn(2:end));
    b(1)=b(1)+us/h^2;
    b(n+1)=b(n+1)/2 + l/h;

    %matrice e termine modificate per newman;
    d0=[d0;1/h^2+q/2];
    dl=[dl;-1/h^2];
    du=[du;-1/h^2];
    
    
    %[T,U,u]=thomas3diag(du,dl,d0,b);
    u=A\b;
    uu=[us;u];
    
    err=[err max(abs(uesatta(xn)'-uu))];
end

figure(1)
plot(xn,uu,'^g')
grid on
hold on
plot(xn,uesatta(xn),'linewidth',2)

figure(2)
loglog(H,err,H,H.^2)
legend('Mio','h^2')
grid on