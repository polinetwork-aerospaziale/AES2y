% Problema con i dati di Neumann - Secondo ordine
%--------------------------------------------------------------------------

clear all
close all
clc

% coefficiente di reazione
q = 3;

% Forzante
f = @(x) (5/4 + q).*sin(x/2).*cos(x) + cos(x/2).*sin(x);

% Soluzione esatta
y_es = @(x) sin(x/2).*cos(x);
% Derivata della soluzione esatta
dy_es = @(x) 0.5*cos(x/2).*cos(x) -  sin(x/2).*sin(x);

% Estremi
a1 = -4;
b1 = 4;

% Dati al bordo
us = y_es(a1);
l = dy_es(b1);

% Ampiezza h dei sottointervalli
h = 0.05;

% Nodi interni
n = (b1-a1)/h -1;    %(Intervalli n+1 - Nodi totali n+2)

% Costruzione nodi (TUTTI)
xn = a1:h:b1;

%% Costruzione del sistema lineare con dato al bordo del primo ordine

%% Matrice A 

d0 = zeros(n+1,1);              % Diagonale principale
d0(1:n) = 1/(h^2)*(2 + h^2*q);
d0(n+1) = 1/(h^2)+q/2;
d1 = -1/(h^2)* ones(n,1);
A = (diag(d0) + diag(d1,1) + diag(d1,-1));

% termine noto b
b = f(xn(2:end));
b(1) = b(1)+us/(h^2);
b(n+1) = b(n+1)/2 + l/h;

% Risoluzione del sistema tridiagonale con Thomas
[Lt,Ut,u] = thomas3diag(d1,d1,d0,b); 

% Assemblamento della soluzione totale
uu = [us; u];

figure()
plot(xn,uu,'o-',xn,y_es(xn),'linewidth',2)
xlabel('x')
ylabel('u(x)')

% Risoluzione al variare di h
H = [0.5 0.25 0.125 0.0625 0.03125];
Err = [];
for h = H
    a1 = -4;
    b1 = 4;
    % Nodi interni
    n = (b1-a1)/h -1;    %(Intervalli n+1 - Nodi totali n+2)
    % Costruzione nodi (TUTTI)
    xn = a1:h:b1;
    %% Matrice A

    d0 = zeros(n+1,1);              % Diagonale principale
    d0(1:n) = 1/(h^2)*(2 + h^2*q);
    d0(n+1) = 1/(h^2)+q/2;

    d1 = -1/(h^2)* ones(n,1);

    A = (diag(d0) + diag(d1,1) + diag(d1,-1));
    title('Matrice del problema ai limiti','fontsize',16, 'color', 'blue')

    % termine noto b
    b = f(xn(2:end));
    b(1) = b(1)+us/(h^2);
    b(n+1) = b(n+1)/2 + l/h;

    % Risoluzione del sistema tridiagonale con Thomas
    [Lt,Ut,u] = thomas3diag(d1,d1,d0,b);

    % Assemblamento della soluzione totale
    uu = [us; u];
    y_es(xn);
    err = max(abs(uu-y_es(xn)'));
    Err = [Err err];

end

figure
loglog(H,Err,'o-',H,H,H,H.^2,'linewidth',2)
grid on
legend('Errore','h','h^2')
title('Errore','fontsize',16,'color','red')

