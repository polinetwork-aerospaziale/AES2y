function [xsol,it]=Tema1newton(fun,dfun,x0,toll,nmax)

if nargin==3;
    toll=1e-5;
    nmax=100;
end

x=x0;
xsol=[];
err=toll+1;
it=0;

while (err>toll && it<nmax)
    if (dfun(x)==0)
        error('Azzeramento della derivata')
    end
    xnew=x-fun(x)/dfun(x0);
    err=abs(x-xnew);
    xsol=[xsol;x];
    
    %aggiorno le componenti
    it=it+1;
    x=xnew;
    
end
