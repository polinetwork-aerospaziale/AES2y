%% TemaB
clear all
close all
clc
%% Es1
%% Domanda 1
n=10;
A=1./pascal(n);
xes=ones(n,1);%termine noto
b=A*xes;%soluzione

%% Domanda 2
for i=1:n-1;
    if(det(A(1:i,1:i))==0)
        disp('condizione non rispettata')
    end
end

%% Domanda 3
[L,U,P]=lu(A);
spy(P);

%% Domanda 4
y=forward(L,P*b);
x=backward(U,y);
cond(A)
err=norm(x-ones(n,1))/norm(xes)
res=norm(A*x-b)/norm(b)

%% Es 2

clear all
close all
clc

%% Domanda 1
f=@(x)x-exp(cos(x));
a=0;
b=pi;
xval=linspace(a,b,1000);
y=f(xval);

figure(1)
plot(xval,y,xval,zeros(size(xval)));

%% Domanda 2- vedi ptfisso.m;
%% Domanda 3 - valori di beta ammissibili
%% Domanda 4
x0=1;
beta=-0.5;
toll=1e-6;
nmax=1000;
phi=@(x)(1+beta)*x-beta*exp(cos(x));

[xsol,it]=ptfisso(x0,nmax,toll,phi);

%% Domanda 5
toll=1e-6;
beta=[-1:0.01:1];
for i=1:length(beta);
    phi2=@(x)((1+beta(i)).*x)-beta(i).*exp(cos(x));
    [xsol2,it2(i)]=ptfisso(1,1000,toll,phi2); 
end

figure(2)
plot(beta,it2)

%% ES 3

clear all
close all
clc

%% Domanda 1
a=-pi;
b=pi;

f=@(x)sin(4*x);

x=[-pi:pi/1000:pi];
y=f(x);

xnodi=[-pi:pi/5:pi];
y1=interp1(xnodi,f(xnodi),x);
y2=cubicspline(xnodi,f(xnodi),x);

err1=max(abs(y2-y));
err2=max(abs(y2-y));

figure(4)
plot(x,y,'linewidth',2)
hold on
plot(x,y1,'r')
plot(x,y2,'g')
legend('soluzione esatta','interpolazioni lin a tratti','cubiche naturali')

%% Ese 4
clear all
close all
clc
%% Domanda 1 foglio
%% Domanda 2 foglio
%% Domanda 3
a=(1/11).*[18 -9 +2];
b=(1/11).*[6 0 0 0];
stabreg(a,b);

%% Domanda 4
f=@(t,y)-2*t.*y.^2;
df=@(t,y)-4*t.*y;
t0=0;
tf=3;
y0=1;
yes=@(t)1./(1+t.^2);
h=0.25;

[t,u] = multistep (a', b', 3, t0+[0 h 2*h]', yes(t0+[0 h 2*h]'), h, f, df, 1e-5, 1000);
%% Domanda 5
H=[0.25 0.1 0.05 0.01];
for i=1:length(H)
    h=H(i);
    [t2,u2] = multistep (a', b', 3, t0+[0 h 2*h]', yes(t0+[0 h 2*h]'), h, f, df, 1e-5, 1000);
    err(i)=max(abs(u2-yes(t2)));
end

figure(2)
loglog(H,err,H,H)

%% Esercizio 5;
clear all
close all
clc

A=1./[1 2 3 4; 2 3 4 5; 3 4 5 6;4 5 6 7];
N=4;
B=10.*eye(N)-diag(ones(N-1,1),1)-diag(ones(N-1,1),-1);
nmax=10000;
cond(A);
cond(B);
toll=1e-6;
spy(B)
[l,it]=qr1(A,toll,nmax)
[l2,it2]=qr1(B,toll,nmax)

