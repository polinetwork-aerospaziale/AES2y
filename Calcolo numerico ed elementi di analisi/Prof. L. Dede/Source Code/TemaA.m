%% Tema A
clear all
close all 
clc

%% ESE1domanda 1-foglio
%% ESE1domanda 2

n=30;
A=diag(2:2:2*n)+diag(1./(2:n),1)+diag(1:(n-1),1);
xes=ones(n,1);
b=A*xes;

%% ESE1domanda 3 -foglio
%% ESE1domanda 4 -foglio
%% ESE1domanda 5
x0=(5:5:5*n)';
toll=10^-5;

[xvettore,iteraz]=jacobi(A,b,x0,toll);

err_relativo=norm(xvettore-xes)./norm(xes);
residuo=norm(b-A*xes)/norm(b);
cond(A);

clear all
%% ESE2
%% domanda 1 -foglio
%% domanda 2 -funciontpuntomedio
f=@(x)sin(x);
a=0;
b=(3/4)*pi;
N=74;
%% domanda 3  
I=pmedcomp(a,b,N,f);

%% domanda 4
Iesatto=1+(2^0.5)/2;
err=abs(I-Iesatto);

clear all
%% Ese3
%% domanda 1 algoritmo di EI;
%% domanda 2
u=@(x)sin(x).*exp(-x/2);
f=@(x,y)cos(x)*exp(-x/2)-0.5*y;
h=0.5;
xval=linspace(0,10,1000);
X=u(xval);
[t,u]=euleroimpli(f,10,0,h);
figure(1)
plot(xval,X);
hold on
plot(t,u)%il grafico fa schifo per la pessima discretizzazione
%% domanda 3 -foglio
%% domanda 4 -foglio
%% domanda 5 -foglio

%% Ese 4
clear all
close all
clc
%% domanda 1
fo=@(x)sin(exp(x)).*exp(x).^2-cos(exp(x)).*exp(x);
uex=@(x)sin(exp(x));
duex=@(x)cos(exp(x)).*(exp(x));

N=[20 40 80 120];
err=[];
H=[];

for n=N
    a=-3;
    b=+0.5;
    
    h=(b-a)./(n+1);
    H=[H;h];
    
    xn=linspace(a,b,n+2);
    
    d0=(2/h^2)*ones(n,1);
    du=(-1/h^2)*ones(n-1,1);
    dl=du;

    s=duex(a);%NEUMAN
    ud=uex(b);%DIRICHLET
    
    d0=[1/(h^2);d0];
    du=[-1/(h^2);du];
    dl=[-1/(h^2);dl];
    
    b=fo(xn(2:end-1));
    b(1)=b(1);
    b(end)=b(end)+ud/h^2;
    
    b=[-s./h,b];
    
    %[L,U,u]=thomas3diag(du,dl,d0,b);
    A=diag(d0)+diag(du,1)+diag(dl,-1);
    u=A\b';
    uu=[u;ud];
    err=[err;max(abs(uex(xn)'-uu))];
    
end

figure(2)
plot(xn,uex(xn),'linewidth',2)

figure(3)
loglog(H,err,H,H,H,H.^2);
grid on
legend('Err','H','H^2')