function [lambda,x,iter]=invpowershift(A,u,tol,nmax,x0)


[n,m]=size(A);

if (n~=m); 
    error('Solo matrici quadrate'); 
end

if (nargin== 2)
   tol=1.e-06;   
   x0=ones(n,1);   
   nmax=100;
end
M=A-u*eye(n) ;
[L,U,P]=lu(M);

iter = 0;
y = x0/norm(x0);
lambda = y'*A*y;
err = tol*abs(lambda) + 1;

while (err>tol*abs(lambda)) && (abs(lambda)~=0) && (iter<nmax)
   iter = iter + 1;
   z=forward(L,P*y);
   x=backward(U,z);
   y=x/norm(x);
   
   new=y'*A*y;
   err=abs(new-lambda);
   lambda=new; 
end