%% Tema di calcolo
clear all
close all
clc

%% Esercizio 1

%% domanda 1

f=@(x)x+sin(2.*x);
df=@(x)1+cos(2.*x).*2;

a=-2;
b=+3;
xval=linspace(a,b,1000);

y=f(xval);
dy=df(xval);

figure (1)
plot(xval,y,xval,dy,'linewidth',2)
grid on
legend('f(x)','derivata f(x)')

disp('press any key to continue')
pause
%% Domanda 2 - vedi newtontema1
%% Domanda 3
close all
df2=@(x)-4*sin(2*x);
x0=0.5;
[xsol,iterazione]=Tema1newton(df,df2,x0)

disp('il massimo della derivata e''');
xsol(end)

%% Esercizio 2
disp('premi un tasto per continuare')
pause

clear all
close all
clc

%% Domanda 1 foglio
%% Domanda 2 
f=@(x)x-exp(1./(1+x.^2));
xval=linspace(-2,2,1000);

a=-2;
b=+2;
n=6;

x6=linspace(a,b,n+1);
y6=f(x6);
p6=polyfit(x6,y6,n);

%% Domanda 3
i=[0:n];

xc=((a+b)/2)+((b-a)/2).*(-cos(pi.*i/n));
yc=f(xc);
pc=polyfit(xc,yc,n);

%% Domanda 4
pp6=polyval(p6,xval);
pcc=polyval(pc,xval);

figure(1)
plot(xval,f(xval),xval,pp6,xval,pcc);
grid on
legend('funzione','polinomio lagrange','polinomio chebishev')

%% Domanda 5 foglio
%% Domanda 6

Err6=abs(f(xval)-pp6);
Errc=abs(f(xval)-pcc);

figure(2)
plot(xval,Err6,xval,Errc)
grid on
legend('errore lagrange','errore cheb')

%% Domanda 7


N=[8 12 16 20];
errn=[];
for n=N
    xn=linspace(a,b,n+1);
    yn=f(xn);
    pn=polyfit(xn,yn,n);
    yn=polyval(pn,xval);
    
    errn=[errn;max(abs(yn-f(xval)))];
end

figure(3)
plot(N,errn)
grid on
legend('errore a nodi equispaziati variabili')


disp('premi un tasto per avanzare al terzo esercizio')
pause
%% Esercizio 3
clear all
close all
clc

%% Domanda 1
%% Domanda 2
%% Domanda 3 power
%% Domanda 4

A=[15 -2 2
   1 10 -3
   -2 1 0 ];

x0=[1/3,1/3,1/3]';
[l1,x1,it1]=Tema1pontenze(A,x0);
disp('il massimo autovettore e''')
max(x1)
disp('il massimo autovalore e''')
max(l1)

%% Esercizio 4
disp('premi un tasto per avanzare')
pause

close all
clear all
clc

%% Domanda 1
%% Domanda 2 
%schema con dirichtlet 
%centrato di ordine 2
xa=1;
xb=2;

ua=1;
ub=1;
f=@(t)4*pi^2*sin(2*pi*t);

h=0.1;
gr=xa:h:xb;

N=(xb-xa)/h -1;

d0=-ones(N,1);
de=-ones(N,1);
d0=2*ones(N,1);

A=spdiags((1/h^2)*[de,d0,de],-1:1,N,N);

b=f(gr(2:end-1));

b(1)=b(1)+ua/h^2;
b(end)=b(end)+ub/h^2;

x=A\b';
x=[ua;x;ub];

%% Domanda 3

y=@(t)1+sin(2*pi*t);
dt=xa:0.01:xb;
ues=y(dt);
figure(4)
plot(dt,ues,'r')
hold on
grid on
plot(gr,x,'-ob')

%% Domanda 4

hvett=[0.1 0.05 0.025 0.0125];
err=[];

for h=hvett
    gr=xa:h:xb;
    
    N=1/h -1;
    d0=2*ones(N,1);
    de=-ones(N,1);
    A=spdiags((1/h^2)*[de,d0,de],-1:1,N,N);
    
    b=f(gr(2:end-1));
    b(1)=b(1)+ua/h^2;
    b(end)=b(end)+ub/h^2;
    
    x=A\b';
    x=[ua;x;ub];
    
    err=[err,max(abs(x'-y(gr)))];
end

figure(6)
loglog(hvett,err,'-or','LineWidth',2)
hold on
grid on
loglog(hvett,hvett.^2,'--b','LineWidth',2);
loglog(hvett,hvett,'--k','LineWidth',2);
legend('errore','h^2','h')