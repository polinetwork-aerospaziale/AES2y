%labo12ese2
clear all
close all
clc

%domanda 1
s=1000*[0.18 0.3 0.5 0.6 0.72 0.75 0.8 0.9 1.0];
e=[0.0005 0.001 0.0013 0.0015 0.002 0.0045 0.006 0.007 0.0085];

%figure(1)
%plot(s,e)
%grid on

%polilagrange
xval=linspace(min(s),max(s),1000);
grad=length(s)-1;
nodi=polyfit(s,e,grad);
polilag=polyval(nodi,xval);

%interpolante composita lineare
poliint=interp1(s,e,xval);

%minimiquadrati
poliquad1=polyval(polyfit(s,e,1),xval);
poliquad2=polyval(polyfit(s,e,2),xval);
poliquad3=polyval(polyfit(s,e,4),xval);





