function[th,uh]=euleroimpli(f,T,y0,h)

t0=0;
th=t0:h:T;
N=length(th);
uh=zeros(1,N);
toll=1e-6;
uh(1)=y0;
N_max=100;
for it=2:N
    phi=@(u)uh(it-1)+h*f(th(it),u);
    [u_pf,it_pf] = ptofis2(uh(it-1),phi,N_max,toll);
    uh(it)=u_pf(end);
end