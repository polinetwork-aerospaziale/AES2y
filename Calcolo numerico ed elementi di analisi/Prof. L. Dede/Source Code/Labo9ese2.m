%labo9 ese2
clear all
close all
clc
%creazione matrice
n=100;
r1=1;
r2=-2;
%assemblo a
A=diag(r2*ones(n,1))+diag(r1*ones(n-1,1),-1);
A(1,:)=1;
%controllo gli elementi nulli
elnul=nnz(A);
%salvo a come una sparsa
Asp=sparse(A);
%controllo a e asp
whos A Asp

%metodo di gaus
[L,U]=lugas(A);
%figure(1)
%spy(L)
%figure(2)
%spy(U)

%domanda 3
Dinv=diag(1./diag(A));
Bj=eye(n)-Dinv*A;
T=tril(A);
Bgs=eye(n)-inv(T)*A;
%trovo i raggi spettrali
rhogs=max(abs(eig(Bgs)))
rhoj=max(abs (eig(Bj)))

%domanda 4 algortimo di jacobi
%domanda 5 eseguo jacobi

b=ones(n,1);
b(1)=2;
x0=zeros(n,1);

[xnew,k]=jacobi(A,b,x0);

k