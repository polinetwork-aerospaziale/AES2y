%%labo18
clear all
close all
clc

%% esercizio 1

%domanda 2
a=[1 0 0]';
b=(1./12).*[0 23 -16 5]';
stabreg(a,b);

%domanda 3
h=0.1;

fun=@(t,y)y./(1+t)-y.^2;
yes=@(t)2.*(1+t)./(t.^2+2.*t+2);
t1=linspace(0,100,1000);
t0=[0 h 2*h]';
u0=yes(t0);
[t,u]=multistep(a,b,100,t0,u0,h,fun);

figure(1)
plot(t,u,'^r')
hold on
plot(t1,yes(t1),'Linewidth',3)

%domanda 4

ERR=[];

H = [ 0.1 0.05 0.025 0.0125];
for h=H
    t0=[0 h 2*h]';
    u0=yes(t0);
    [t,u]=multistep(a,b,100,t0,u0,h,fun);
    ERR=[ERR max(abs(u-yes(t)))];
end

close all
figure(1)
loglog(H,ERR,H,H.^3,H,H.^2)
legend('metodo','3 ordine','2 ordine')