%% Labo 20
clear all
close all
clc

%% Domanda 1
aa=-4;
bb=+4;
q=3;

uesatta=@(x)sin(x./2).*cos(x);

us=uesatta(aa);
ud=uesatta(bb);

H=[0.5 0.25 0.125 0.0625 0.03125];
err=[];
for h=H
    n=8/h-1;
    xn=-4:h:4;
    xv=linspace(-4,4,100);
    u=zeros(1,n);
    dupper=-1*ones(n-1,1)/h^2;
    dlower=dupper;
    
    f=(5/4+q).*sin(xn(2:end-1)'./2).*cos(xn(2:end-1)')+cos(xn(2:end-1)'./2).*sin(xn(2:end-1)');
    
    b=zeros(n,1);
    b(1)=us/h^2;
    b(end)=ud/h^2;
    b=b+f;
    
    d0=((2+q*h^2)*ones(n,1))/h^2;
    [T,U,u]=thomas3diag(dupper,dlower,d0,b);
    uu=[us;u;ud];
    
    
    err=[err max(abs(uesatta(xn)'-uu))];
end
figure(1)
plot(xn,uu,'^g')
grid on
hold on
plot(xn,uesatta(xn),'linewidth',2)

figure(2)

loglog(H,err,H,H.^2,H,H,H,H.^3)
legend('mioerrore','h^2','h')
grid on
