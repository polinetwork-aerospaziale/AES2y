function I = simpcomp(a,b,N,fun)

h=abs(a-b)/N;%vettore intervalli
x=[a:h/2:b];%vettore dei punti
y=fun(x);%valuto la funzione
I=(h/6).*(y(1)+2*sum(y(3:2:2*N-1))+4*sum(y(2:2:2*N))+y(2*N + 1));%simpson composito