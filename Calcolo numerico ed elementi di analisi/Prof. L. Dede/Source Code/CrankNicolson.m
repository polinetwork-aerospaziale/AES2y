function[t,u,it]=CrankNicolson(f,tmax,y0,h)

% vettore degli istanti in cui risolvo la edo

t0=0;
t=t0:h:tmax;

% inizializzo il vettore che conterra' la soluzione discreta

N=length(t);
u=zeros(1,N);

% ciclo iterativo che calcola u^{n+1}=u^n+1/2*h*( f^n +f^{n+1}) . 
% Ad ogni iterazione temporale devo eseguire delle sottoiterazioni di punto fisso 
% per il calcolo di u^{n+1}: 
%
% u_(n+1)^(k+1) = u_n + h/2*( f^n + f_( t_(n+1) , u_(n+1)^k ) ). 
u(1)=y0;
% parametri per le iterazioni di punto fisso
N_max=100;
toll=1e-5;
itpuntofisso=zeros(1,N);



for it=2:N
    % preparo le variabili per le sottoiterazioni
    uvecchia=u(it-1);
    f_old=f(t(it-1),uvecchia);
    t_pf=t(it);   
    phi=@(u) uvecchia+0.5*h*(f_old+f(t_pf,u));
    % sottoiterazioni
    [upf,itpf]=ptofis2(uvecchia,phi,N_max,toll);
    u(it)=upf(end);
    % tengo traccia dei valori di itpf e err per valutare la convergenza delle 
    % iterazioni di punto fisso
    itpuntofisso(it)=itpf;
end