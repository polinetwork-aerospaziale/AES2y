%labo 10
clear all
close all
clc
%ese1

%domanda 1
n=50;
d1=diag(2*ones(n,1));
d2=diag(-1*ones(n-1,1),-1);
d3=diag(-1*ones(n-1,1),+1);
b=0.2*ones(n,1);
x0=zeros(n,1);
toll=1e-6;
nmax=10000;
n=50;
n2=49;
n3=48;
n4=47;
n5=46;

c1=diag(4*ones(n,1));
c2=diag(-1*ones(n2,1),+1);
c3=diag(-1*ones(n2,1),-1);
c4=diag(-1*ones(n3,1),+2);
c5=diag(-1*ones(n3,1),-2);
A=c1+c2+c3+c4+c5;

P=d1+d2+d3;

%domanda 2
if P==P'
    v=eig(P);
    if v>0
        disp('SDP')
    else
        disp('S')
    end
else
    disp('non S')
end

%domanda 3
v=eig(inv(P)*A);
a=2/(max(v)+min(v));
Balfa=eye(n)-a*inv(P)*A;
rs=max(abs(eig(Balfa)))
nc=cond(inv(P)*A)

[x,it]=richy(A,b,P,x0,toll,nmax,a)

[x2,it2]=richy(A,b,P,x0,toll,nmax)

    