%%Esercizio 2
clear all
close all
clc
%intervallo
a=0.5;
b=7;
%funzioni
f=@(x) x.^3 - (2+exp(1))*x.^2 + (2*exp(1)+1)*x + (1-exp(1)) - cosh(x-1);
fprimo=@(x) 3*x.^2 - 2*(2+exp(1))*x + (2*exp(1)+1) - sinh(x-1);
fsecondo=@(x) 6*x - 2*(2+exp(1)) - cosh(x-1);
x=linspace(a,b,100);
%definizione funzioni
y=f(x);
y1=fprimo(x);
y0=zeros(size(x));
%disegno
figure(1)
plot(x,y,x,y1,x,y0);
grid on

%radici semplici e non
a1 = 1;
if (fprimo(a1) == 0)
    if (fsecondo(a1) == 0)
        fprintf('> due\n')
    else
        fprintf('= due\n')
    end
else
    fprintf('= uno\n')
end

%newton
x=6;
nmax=1000;
toll=10^-3;
[xvect1,it1]=newton(x,nmax,toll,f,fprimo)
%netwon2
x2=3;
nmax=1000;
toll=10^-3;
[xvect2,it2]=newton(x2,nmax,toll,f,fprimo)
