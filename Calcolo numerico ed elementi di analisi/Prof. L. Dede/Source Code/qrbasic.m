function D=qrbasic(A,tol,nmax)

if (nargin==1)
    tol=1e-6;
    nmax=1000;
end

[n,m]=size(A)
if(n~=m)
    error('only square matrix')
end
 T=A;
 n=0;
 err=max(max(abs(tril(T,-1))));
 
 while(n<nmax)&&(err>tol)
     [Q,R]=qr(T);
     T=R*Q;
     err=max(max(abs(tril(T,-1))));
     n=n+1;
 end
 n
 D=diag(T);