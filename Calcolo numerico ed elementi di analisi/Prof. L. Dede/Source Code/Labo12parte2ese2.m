%labo12parte2ese2
clear all
close all
clc
%domanda 1
f=@(x)1./(1+x.^2);
x=linspace(-5,5,1000);
y=f(x);

%lagrange
N=[5,10];
Fl=[];
for n=N
    punti=n+1;
    nodi=linspace(-5,+5,punti);
    fnodi=f(nodi);
    
    P=polyfit(nodi,fnodi,n);
    polilag=polyval(P,x);
    Fl=[Fl;polilag];
end

err=abs(Fl-[y;y]);
figure(1)
plot(x,err)

%domanda 2 cheby
a=-5;
b=+5;
N=[5 10];
E=[];
for z=N
    i=[0:z];
    xi=-cos((pi/z)*i);
    nodi=(a+b)/2+((b-a)/2)*xi;
    
    P=polyfit(nodi,f(nodi),z);
    poliche=polyval(P,x);
    E=[E;poliche];
end

errche=abs(E-[y;y]);

figure(2)
plot(x,errche)

