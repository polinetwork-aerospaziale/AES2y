function [i]=Tema2puntomedio(a,b,n,f)
h=abs(b-a)/n;
m=[a+h/2:h:b-h/2];
i=h*sum(f(m));