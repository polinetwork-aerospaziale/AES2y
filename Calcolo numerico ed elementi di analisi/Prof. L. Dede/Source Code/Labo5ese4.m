%%Lab5ese4

clear all
close all
clc
%definizione funzione
f=@(x)(cos(2*x)).^2-x.^2;
a=-pi/2;
b=+pi/2;
x=linspace(a,b,100);
y=f(x);
y0=zeros(size(x));
%disegno figura
%figure(1)
%plot(x,y,'r',x,y0,'g')
%grid on

%richiesta 3
A=0.1;
toll=10^-10;
x0=0.1;
nmax=1000;
%funzioni
phi=@(x)x+A*((cos(2*x)).^2-x.^2);
phi1=phi(x);
%grafico
figure(2)
plot(x,y,'g',x,phi1)
grid on
%puntofisso
[succ,it]=puntofissoprof(x0,phi,nmax,toll,a,b)

%trovare il valore della funzione
df = @(x) -4*cos(2*x).*sin(2*x)-2*x;
df1=df(succ(end));
Asup=-df1/2;

A2=0.6;
phi2 = @(x) x + A2*(cos(2*x).^2 - x.^2);
[succ2,it2] = puntofissoprof(x0,phi2,nmax,toll,a,b)

