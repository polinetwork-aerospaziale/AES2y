%Laboratorio 12 esercizio 1
clear
close all
clc

%% punto 1
fun = @(x) x .* sin(x);
a = -2;  b = 6;
x_dis = linspace( a, b, 1000 );
f_dis = fun(x_dis);

fplot(fun,[-2,6])

%% punti 2 e 3
PP_dis = [];
err_dis = [];
err_max = [];

for n=2:2:6
    h = ( b - a ) / n;
    x_nod = [ a : h : b ];
    f_nod = fun(x_nod);
    P = polyfit( x_nod, f_nod, n )
    P_dis = polyval( P, x_dis );
    PP_dis = [ PP_dis; P_dis ];
    err_dis = [ err_dis;  abs( P_dis - f_dis ) ];
    err_max = [ err_max; max( abs( P_dis - f_dis ) ) ];   
  
end

figure(1)
plot( x_dis, f_dis, 'r')
grid on
hold on
plot( x_dis, PP_dis(1,:),'g')
legend('y=x*sin(x)','pol2')
xlabel('asse x')
ylabel('asse y')
title('f(x)=x*sin(x) e polinomio interpolatore di secondo grado')

figure(2)
plot( x_dis, f_dis, 'k')
hold on
grid on
plot( x_dis, PP_dis)
legend('y=xsin(x)','pol 2','pol 4','pol 6')
xlabel('asse x')
ylabel('asse y')
title('f(x)=xsin(x) e suoi polinomi interpolanti')

figure( 3 )
plot( x_dis, err_dis )
grid on
legend('pol 2', 'pol 4', 'pol 6')
xlabel('asse x')
ylabel('asse y')
title('Errore di interpolazione')
err_max
