close all
clear all


f = @(x) exp(1-5*x).*(x+1).^2;
a = 0; b = 4;

H_vect = 0.1*2.^-[0:3];
E_H_vect = [];
for k = 1:length(H_vect)
   H = H_vect(k);
   x = [a:H:b];
   pz = interp1(x, f(x), x);
   
   
   z = linspace(a,b,1000);
   pz = interp1(x, f(x), z);
   E_H = max( abs(f(z)-pz) )
   
   plot(z,f(z),z,pz)
   
   
E_H_vect = [E_H_vect E_H];
   
end

loglog(H_vect, E_H_vect)


% %% e
% close all
% clear all
% x = [0:4];
% y = [0 1 32 243 1024];
% p = polyfit(x,y,1)