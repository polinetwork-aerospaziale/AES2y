close all
clear all

%% 2
close all
clear all
x = [0 1 3 7];
y = [-1 0 0 1];
p = polyfit(x,y,3);
pz = polyval(p, 5)


%% 3
close all
clear all
f = @(x) 3*x.^3;
a = 1; b = 7;

I = (b-a)*f((a+b)/2)

%% 4
close all
clear all
tol = 1e-5;
d2f = @(x) (-pi^2)*sin(pi*x);
x = [0:0.0001:1];
M = ceil (sqrt( (1/(12*tol)) * max(abs(d2f(x))) ) )