close all
clear all

x0 = 0; xf = pi/2;
alpha = 3; beta = 0;
f = @(x) -3*exp(-3*x).*(8*cos(x)+6*sin(x));
u_ex = @(x) 3*exp(-3*x).*cos(x);

N_vect = [9 19 39 79];

mu = 1;

e_vect = [];
for k = 1:length(N_vect)
N = N_vect(k);
h = (xf-x0)/(N+1);
x = [x0+h:h:xf-h]';
A = (mu/(h^2)) * (diag(-ones(N-1,1),-1) + 2*eye(N) + diag(-ones(N-1,1),1) );
b = f(x);
b(1) = b(1) + alpha/(h^2);
b(end) = b(end) + beta/(h^2);
u = A\b;

e = max(abs(u_ex(x) - u));
e_vect = [e_vect e];

end

logloge_vect
