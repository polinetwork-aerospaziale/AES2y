function [x, k] = fixedPt(x0, phi, itermax, tol)

x = x0;

succ = [];

k = 0;

err = tol + 1;

while (err>tol) & (k < itermax)
    
    k = k +1;
    
    x_old = x;
    x = phi(x);
    succ = [succ x];
    
    err = abs(x-x_old);
    
    
end

end

