function [t_h, u_h] = heun(f, t0, tf, y0, h)


t_h = [t0:h:tf];

u_h = [y0];
N_h = (tf-t0)/(h);

for n = 0:N_h-1
    
    t_n = n*h;
    t_np1 = t_n + h;
    u_n = u_h(end);
    
    u_np1s = u_n + h*f(t_n,u_n);
    u_np1 = u_n + (h/2)*(f(t_n,u_n) + f(t_np1, u_np1s));
    
    u_h = [u_h; u_np1];
end



return

