function x=bksub(U,b)

% function [x] = bksub(A,b)
% Algoritmo di sostituzione all'indietro
% A: matrice quadrata triangolare superiore
% b: termine noto
% x: soluzione del sistema Ax=b

n=length(b);

if((size(U,1)~=n)||(size(U,2)~=n))
  error('ERRORE: dimensioni incompatibili')
end

if(~isequal(U,triu(U)))
  error('ERRORE: matrice non triangolare superiore')
end


if(prod(diag(U))==0)
% almeno un elemento diagonale nullo
  error('ERRORE: matrice singolare')
end

x=zeros(n,1);

x(n) = b(n)/U(n,n);

for i=n-1:-1:1
   x(i)=(b(i)-U(i,i+1:n)*x(i+1:n))/U(i,i);
end

